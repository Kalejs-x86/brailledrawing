import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class MainWindow extends JApplet
        implements MouseListener, MouseMotionListener, ActionListener
{
    TextDrawing DrawPanel; // JPanel derivative
    JTextArea OutputBox;
    JCheckBox InverseCheck;

    @Override
    public void init()
    {
        addMouseListener(this);
        addMouseMotionListener(this);

        setLayout(new GridLayout());

        DrawPanel = new TextDrawing();
        add(DrawPanel, BorderLayout.CENTER);

        JPanel Controls = new JPanel();

        JButton GenBtn = new JButton("Generate");
        GenBtn.addActionListener(this);
        Controls.add(GenBtn);

        JButton ClearBtn = new JButton("Clear");
        ClearBtn.addActionListener(this);
        Controls.add(ClearBtn);

        InverseCheck = new JCheckBox("Inverse");
        Controls.add(InverseCheck);

        OutputBox = new JTextArea(20, 40);
        OutputBox.setEditable(false);
        Controls.add(OutputBox);

        add(Controls, BorderLayout.EAST);
    }

    @Override
    public void start(){}


    @Override
    public void mousePressed(MouseEvent e)
    {
        int Button = e.getButton();
        DrawPanel.SetMouseButtonActive(Button);

        DrawPanel.MouseInput(e);
        repaint();
    }

    public void mouseEntered( MouseEvent e ) {
    }
    public void mouseExited( MouseEvent e ) {
    }
    public void mouseClicked( MouseEvent e ) {
    }

    public void mouseReleased( MouseEvent e ) {  // called after a button is released
        int Button = e.getButton();
        DrawPanel.SetMouseButtonInactive(Button);
    }
    public void mouseMoved( MouseEvent e ) {  // called during motion when no buttons are down
    }
    public void mouseDragged( MouseEvent e ) {  // called during motion with buttons down
        DrawPanel.MouseInput(e);
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getActionCommand() == "Generate")
        {
            String Text = DrawPanel.GenerateMessage(InverseCheck.isSelected());
            OutputBox.setText(Text);
        }
        if (e.getActionCommand() == "Clear")
        {
            DrawPanel.Clear();
            OutputBox.setText("");
            repaint();
        }
    }
}
