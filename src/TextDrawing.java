import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

public class TextDrawing extends JPanel
{
    static class Style{
        public static final Color COLOR_BACKGROUND = new Color(0, 0, 0);
        public static final Color COLOR_OUTLINE = new Color(0, 0, 0);
        public static final Color COLOR_FILLED = new Color(0, 0, 0);
        public static final Color COLOR_EMPTY = new Color(255, 255, 255);
    }

    /*
     * Terms explained
     * SQUARE = The smallest unit the user can fill in.
     * 1 braille symbol is composed of 2x4 dots, filled or not...
     * but since they will not be rendered as circles/dots but instead as rectangles...
     * they are referred to as SQUARE
     *
     * BLOCK = A collection of SQUARES which represent a braille character
     *
     * GRID = The 'canvas', composed of hundreds of SQUARE's for drawing, which will be...
     * grouped into BLOCKs for converting into braille
     */

    private final int BLOCK_WIDTH = 2;
    private final int BLOCK_HEIGHT = 4;

    private final int IMAGE_RES_WIDTH = 40;
    private final int IMAGE_RES_HEIGHT = 20;

    private final int SQUARE_WIDTH = 5;
    private final int SQUARE_HEIGHT= 5;

    private final int GRID_OFFSET_X = 0;
    private final int GRID_OFFSET_Y = 0;

    private boolean[][] Grid = new boolean[IMAGE_RES_WIDTH*BLOCK_WIDTH][IMAGE_RES_HEIGHT*BLOCK_HEIGHT];
    private boolean LeftMBActive = false;
    private boolean RightMBActive = false;

    /*======================
            Methods
    ======================*/

    private Rectangle GetGridRect()
    {
        Rectangle Grid = new Rectangle();
        Grid.x = GRID_OFFSET_X;
        Grid.y = GRID_OFFSET_Y;
        Grid.width = IMAGE_RES_WIDTH*BLOCK_WIDTH*SQUARE_WIDTH;
        Grid.height = IMAGE_RES_HEIGHT*BLOCK_HEIGHT*SQUARE_HEIGHT;
        return Grid;
    }

    private boolean IsCursorOnGrid(Point CursorPos)
    {
        Rectangle Grid = GetGridRect();
        return Grid.contains(CursorPos);
    }

    private Point GetSquareAtCursor(Point CursorPos)
    {
        Point RelativePos = CursorPos;
        RelativePos.x -= GRID_OFFSET_X;
        RelativePos.y -= GRID_OFFSET_Y;

        Point BlockIndex = new Point();
        BlockIndex.x = (int)Math.floor(RelativePos.x / SQUARE_WIDTH);
        BlockIndex.y = (int)Math.floor(RelativePos.y / SQUARE_HEIGHT);
        return BlockIndex;
    }

    private char ToBraille(int Number)
    {
        int Offset = 0x2800; // Start of braille charset
        return (char)(Offset+Number);
    }

    /*==========================================*/

    public void Clear()
    {
        for (int x = 0; x < Grid.length; x++)
        {
            for (int y = 0; y < Grid[0].length; y++)
            {
                Grid[x][y] = false;
            }
        }
    }

    public String GenerateMessage(boolean Inverse)
    {
        final boolean MatchCondition = Inverse;
        StringBuilder sb = new StringBuilder();

        // Iterate block by block
        for (int y = 0; y < Grid[0].length; y += BLOCK_HEIGHT)
        {
            for (int x = 0; x < Grid.length; x += BLOCK_WIDTH)
            {
                int BrailleNum = 0;

                // Get braille number from filled squares
                if (Grid[x+0][y+0] == MatchCondition) BrailleNum += 1;
                if (Grid[x+0][y+1] == MatchCondition) BrailleNum += 2;
                if (Grid[x+0][y+2] == MatchCondition) BrailleNum += 4;
                if (Grid[x+1][y+0] == MatchCondition) BrailleNum += 8;
                if (Grid[x+1][y+1] == MatchCondition) BrailleNum += 16;
                if (Grid[x+1][y+2] == MatchCondition) BrailleNum += 32;
                if (Grid[x+0][y+3] == MatchCondition) BrailleNum += 64;
                if (Grid[x+1][y+3] == MatchCondition) BrailleNum += 128;

                char Symbol = ToBraille(BrailleNum);
                sb.append(Symbol);
            }
            sb.append('\u0020'); // Youtube comment had that
            sb.append('\n');
        }

        return sb.toString();
    }

    public void MouseInput(MouseEvent e)
    {
        Point CursorPos = e.getPoint();
        if (IsCursorOnGrid(CursorPos))
        {
            Point GridIndex = GetSquareAtCursor(CursorPos);
            if (LeftMBActive)
                Grid[GridIndex.x][GridIndex.y] = true;
            if (RightMBActive)
                Grid[GridIndex.x][GridIndex.y] = false;
        }
    }

    public void SetMouseButtonActive(int Button)
    {
        if (Button == MouseEvent.BUTTON1)
            LeftMBActive = true;
        if (Button == MouseEvent.BUTTON3)
            RightMBActive = true;
    }

    public void SetMouseButtonInactive(int Button)
    {
        if (Button == MouseEvent.BUTTON1)
            LeftMBActive = false;
        if (Button == MouseEvent.BUTTON3)
            RightMBActive = false;
    }

    /*======================
        Event listeners
    ======================*/

    public void paint(Graphics g)
    {
        paintComponents(g);

        // Clear screen
        g.setColor(Style.COLOR_BACKGROUND);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        // Draw grid & squares
        for (int x = 0; x < Grid.length; x++)
        {
            for (int y = 0; y < Grid[x].length; y++)
            {
                if (Grid[x][y])
                {
                    g.setColor(Style.COLOR_FILLED);
                    g.fillRect( 0 + x * SQUARE_WIDTH, 0+ y * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT);
                }
                else
                {
                    g.setColor(Style.COLOR_EMPTY);
                    g.fillRect( 0 + x * SQUARE_WIDTH, 0+ y * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT);
                }

                g.setColor(Style.COLOR_OUTLINE);
                g.drawRect( 0 + x * SQUARE_WIDTH, 0+ y * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT);
            }
        }
    }
}