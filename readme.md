A java applet for creating copy-pastable drawings, using unicode braille characters.

![MainWindow screenshot](img/Window.PNG)

This is a small project I've made, which I originally intended to publish on a website but have now decided to scrap that idea, seeing how modern browsers have stopped supporting Java applications.